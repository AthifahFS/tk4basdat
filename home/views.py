from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Narasumber, Mahasiswa, Dosen, Staf, Universitas
from django.db import connection

# Create your views here.
response={}
def index(request):
  html = 'landingpage.html'
  response['status'] = {'status': 'true'}
  return render(request, html, response)

def returnRegister(request):
  response['status'] = {'status': 'true'}
  html = 'register.html'
  return render(request, html,response)


def register(request):
    if request.method == "POST":
        username = request.POST['name']
        password = request.POST['password']
       	email = request.POST['email']
       	niknpm = request.POST['niknpm']
       	nohp = request.POST['nohp']
       	tanggal = request.POST['tanggal']
       	tempat = request.POST['tempat']
       	radio_value = request.POST.get('radio')
        status = request.POST['status']
        idUniversitas = request.POST['idUniversitas']

        pid = 0;

        for p in Narasumber.objects.raw('SELECT * FROM narasumber;'):
          if int(p.id) > pid:
            pid = int(p.id)


        exist = False
        u = Narasumber.objects.raw('SELECT * FROM NARASUMBER WHERE username=%s;',[username])
        for x in u:
          print(x)
          exist = True
          
        if (exist):
          messages.error(request, "Username sudah terdaftar")
          request.session['status'] = 'true'
          print('AHHAHAHHAHAHHAHAH')
          response['status'] = {'status': 'false'}
        else:
          try:
            with connection.cursor() as cursor:
              cursor.execute("INSERT INTO NARASUMBER(id, nama, email, tempat, no_hp, jumlah_berita, rerata_kata, id_universitas, username, password) VALUES (%s, %s, %s,%s, %s, %s, %s, %s, %s, %s);", [pid+1, username, email, tempat, nohp, 0, 0, 11111, username, password])
              if radio_value == 'mahasiswa':
                cursor.execute("INSERT INTO MAHASISWA(id_narasumber, npm, status) VALUES (%s, %s, %s);", [pid+1, niknpm, status])
              elif radio_value == 'dosen':
                cursor.execute("INSERT INTO DOSEN(id_narasumber, nik_dosen, jurusan) VALUES (%s, %s, %s);", [pid+1, niknpm, 'Ilmu Komputer'])
              else:
                cursor.execute("INSERT INTO STAF(id_narasumber, nik_dosen, posisi) VALUES (%s, %s, %s);", [pid+1, niknpm, 'Staff'])
              request.session['status'] = 'true'
              response['status'] = {'status': 'true'}
          except Exception as e:
            print(e)
    html = 'register.html'
    return render(request, html, response)
