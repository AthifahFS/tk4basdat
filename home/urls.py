from django.conf.urls import url
from .views import index
from .views import returnRegister
from .views import register
from .custom_auth import auth_login, auth_logout

#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^register/$', returnRegister, name='returnRegister'),
    url(r'^custom_auth/register/$', register, name='register'),
]
