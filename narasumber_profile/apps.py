from django.apps import AppConfig


class NarasumberProfileConfig(AppConfig):
    name = 'narasumber_profile'
