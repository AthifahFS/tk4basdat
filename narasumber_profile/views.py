from django.shortcuts import render
from .models import Narasumber, Universitas, NarasumberBerita, Berita

# Create your views here.
response={}
def index(request, id):
	html = 'profile.html'
	narasumber = Narasumber.objects.get(id=id)
	universitas = narasumber.id_universitas
	narasumber_berita = NarasumberBerita.objects.filter(id_narasumber=narasumber)
	daftar_berita = Berita.objects.filter(url__in=narasumber_berita)
	response['narasumber'] = narasumber
	response['universitas'] = universitas
	response['daftar_berita'] = daftar_berita
	return render(request, html, response)
