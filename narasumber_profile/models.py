# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Berita(models.Model):
    url = models.CharField(primary_key=True, max_length=50)
    judul = models.CharField(max_length=100)
    topik = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    update_at = models.DateTimeField()
    jumlah_kata = models.IntegerField()
    rerata_rating = models.FloatField()
    id_universitas = models.ForeignKey('Universitas', models.DO_NOTHING, db_column='id_universitas')

    class Meta:
        managed = False
        db_table = 'berita'


class Dosen(models.Model):
    id_narasumber = models.ForeignKey('Narasumber', models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    nik_dosen = models.CharField(max_length=20)
    jurusan = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'dosen'


class Mahasiswa(models.Model):
    id_narasumber = models.ForeignKey('Narasumber', models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    npm = models.CharField(max_length=20)
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'mahasiswa'


class Narasumber(models.Model):
    id = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    tanggal = models.CharField(max_length=50)
    no_hp = models.CharField(max_length=50)
    jumlah_berita = models.IntegerField()
    rerata_kata = models.IntegerField()
    id_universitas = models.ForeignKey('Universitas', models.DO_NOTHING, db_column='id_universitas')

    class Meta:
        managed = False
        db_table = 'narasumber'


class NarasumberBerita(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    id_narasumber = models.ForeignKey(Narasumber, models.DO_NOTHING, db_column='id_narasumber')

    class Meta:
        managed = False
        db_table = 'narasumber_berita'
        unique_together = (('url_berita', 'id_narasumber'),)


class Staf(models.Model):
    id_narasumber = models.ForeignKey(Narasumber, models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    nik_staf = models.CharField(max_length=20)
    posisi = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'staf'


class Universitas(models.Model):
    id = models.IntegerField(primary_key=True)
    jalan = models.CharField(max_length=100)
    kelurahan = models.CharField(max_length=50)
    provinsi = models.CharField(max_length=50)
    kodepos = models.CharField(max_length=10)
    website = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'universitas'
